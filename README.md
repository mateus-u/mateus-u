## Full Stack Developer
### Main Technologies currently:
- **.Net Core**
	- **.Net Core, .Net Framework, EntityFramework, SqlServer, MySql, OracleSql.**

- **Azure:**
	- **Azure Pipelines, Azure DevOps, CI/CD.**

- **Frontend:**
	- **Angular, Ionic, Scss, TypeScript.**
	
## Professional experience with:
- **Python**
	- **Flask, Django, Pandas and PyQt**
- **C/C++**
	- **Qt, OpenGL, Linux (Thread, Process, Signals, etc)**
- **Arduino**

## Other Programming Languages:
- **Java, Go, Lua, Haskel, Rust, Php**
